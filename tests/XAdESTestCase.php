<?php
declare(strict_types=1);


namespace MG\XAdES\Tests;


use MG\XAdES\CanonicalizationMethod;
use MG\XAdES\Certificate;
use MG\XAdES\Hash\SHA1Hash;
use MG\XAdES\Hash\SHA256Hash;
use MG\XAdES\Object\QualifyingPropertiesObject;
use MG\XAdES\QualifyingProperties;
use MG\XAdES\Reference;
use MG\XAdES\Signature;
use MG\XAdES\SignatureMethod\SHA256Signature;
use MG\XAdES\SignedInfo;
use MG\XAdES\SignedProperties;
use MG\XAdES\Transform\CanonicalizationMethodTransform;
use MG\XAdES\URI\SignedPropertiesURI;
use MG\XAdES\URI\StringURI;
use DateTime;
use PHPUnit\Framework\TestCase;

class XAdESTestCase extends TestCase
{
    public function testCreate()
    {
        $paramsString = "651090185400000001300323162.5404109013040000000113054102PLN\n";
        $private_path = __DIR__ .'/Certificate/key.pem';
        $public_path = __DIR__.'/Certificate/cert.pem';
        $certificate = new Certificate($private_path, $public_path, new SHA1Hash());

        $signedInfo = new SignedInfo(new CanonicalizationMethod(),new SHA256Signature($certificate));

        $sha256hash = new SHA256Hash();
        $reference2 = new Reference(new StringURI('transactions', $paramsString),$sha256hash);
        $signedProperties = new SignedProperties($certificate, $reference2, new DateTime('2017-10-09T10:20:54'), 'application/octet-stream');

        $reference1 = new Reference(new SignedPropertiesURI($signedProperties),$sha256hash);
        $reference1->addTransform(new CanonicalizationMethodTransform());
        $signedInfo
            ->addReference($reference1)
            ->addReference($reference2);

        $signature = new Signature($certificate, $signedInfo);

        $qualifyingProperties = new QualifyingProperties();
        $qualifyingProperties
            ->setTarget($signature)
            ->addSignedProperties($signedProperties);

        $object = new QualifyingPropertiesObject($qualifyingProperties);

        $signature->addObject($object);
//        dump($signature->asXMLString());

        $this->assertEquals('BXkztjJKg/k03wBXzhEG0O3LMBy9vIiUMDcQDi/F9VI=', $reference2->getDigestValue());
        $this->assertEquals('NTXEwdho6DCAjH29uuMxlqq/u6c=', $certificate->get509DigestValue());
/*
        $this->assertEquals('K9SFJUpZPK9aKhDMJqv5YQH9WB8wrr0j+4ZSY0wqYRVuq44UaV3r9cASPJskn2BEABgq7ZWkb78kv9/aHo1uQEhJoifzL23RFNq4U7Bi64Sv3mzwVE4l2yg/skS+GekuqvfHFQxGfSA5fcevmcWuJHjxuXbHwZbT0uG+G7h8kZC+iXH0o87kFZ3KtS2bRMNOFmIIiw2FMXUvulVWYyO0nZiYpae6wmdMiypvZIt1pU/UETRD/nlPRrJ+E0YlzqqXUZUyFgGD9yb7YPfV7gVYQsreg9Ug0bpOfKd92UXGYsEmmE4R74Evl1NDIbDaOE5fFJ7Ow5gfJ8Btvboc9XzOUQ==', $signature->getSignatureValue()->getValue());
        $this->assertEquals('aZBOEKqrUS1fdymBhq0wxbETnBkj/OfeZwScu0qNsvc=', $reference1->getDigestValue());
*/
    }
}