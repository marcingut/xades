<?php
declare(strict_types=1);


namespace MG\XAdES;


use MG\XAdES\Object\ObjectInterface;
use DOMDocument;
use SimpleXMLElement;

class Signature extends dsAbstract
{
    /**
     * @var SignedInfo
     */
    private $signedInfo;
    /**
     * @var Certificate
     */
    private $certificate;

    /**
     * @var ObjectInterface[]
     */
    private $objects=[];
    /**
     * @var SignatureValue
     */
    private $signatureValue;

    /**
     * Signature constructor.
     * @param Certificate $certificate
     * @param SignedInfo $signedInfo
     */
    public function __construct(Certificate $certificate, SignedInfo $signedInfo)
    {
        $this->certificate = $certificate;
        $this->signedInfo = $signedInfo;
        $this->signatureValue = new SignatureValue($signedInfo);

        parent::__construct();
    }

    /**
     * @return SignedInfo
     */
    public function getSignedInfo(): SignedInfo
    {
        return $this->signedInfo;
    }

    /**
     * @return Certificate
     */
    public function getCertificate(): Certificate
    {
        return $this->certificate;
    }

    /**
     * @return SignatureValue
     */
    public function getSignatureValue() : SignatureValue
    {
        return $this->signatureValue;
    }

    /**
     * @param ObjectInterface $object
     * @return $this
     */
    public function addObject(ObjectInterface $object) : self
    {
        $this->objects[]=$object;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeyInfo() : string
    {
        return $this->getCertificate()->get509();
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:Signature xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:Signature>');
        $xml->addAttribute("Id", $this->getId());

        $xml
            ->addChild('ds:KeyInfo')
            ->addChild('ds:X509Data')
            ->addChild('ds:X509Certificate',$this->getCertificate()->get509());

        $dom = $this->convertSimpleXMLElementToDOMDocument($xml);

        $signedInfo = $dom->importNode($this->getSignedInfo()->asXML()->documentElement, true);
        $dom->documentElement->appendChild($signedInfo);

        $signatureValue = $dom->importNode($this->getSignatureValue()->asXML()->documentElement, true);
        $dom->documentElement->appendChild($signatureValue);

        foreach ($this->getObjects() as $object)
        {
            $object = $dom->importNode($object->asXML()->documentElement, true);
            $dom->documentElement->appendChild($object);
        }
        return $dom;
    }

    /**
     * @return ObjectInterface[]
     */
    public function getObjects(): array
    {
        return $this->objects;
    }
}