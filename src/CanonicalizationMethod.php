<?php
declare(strict_types=1);


namespace MG\XAdES;


use DOMDocument;

class CanonicalizationMethod
{
    /**
     * @var string
     */
    private $algorithmUrl= 'http://www.w3.org/2001/10/xml-exc-c14n#';

    /**
     * @return string
     */
    public function getAlgorithmUrl(): string
    {
        return $this->algorithmUrl;
    }

    /**
     * @param string $algorithmUrl
     */
    public function setAlgorithmUrl(string $algorithmUrl): void
    {
        $this->algorithmUrl = $algorithmUrl;
    }

    /**
     * @param DOMDocument $dom
     * @return string
     */
    public function getCanonicalized(DOMDocument $dom) : string
    {
        return $dom->C14N();
    }

}