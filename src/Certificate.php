<?php
declare(strict_types=1);


namespace MG\XAdES;


use MG\XAdES\Hash\HashInterface;
use DOMDocument;
use SimpleXMLElement;

/**
 * Class Certificate
 * @package MG\XAdES
 */
class Certificate extends dsAbstract
{
    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var resource
     */
    private $publicKeyResource;
    /**
     * @var HashInterface
     */
    private $digisetHash;
    /**
     * @var string
     */
    private $publicKey;
    /**
     * @var resource
     */
    private $privateKeyResource;

    /**
     * Certificate constructor.
     * @param string $privateKey
     * @param string $publicKey
     * @param HashInterface $digisetHash
     */
    public function __construct(string $privateKey, string $publicKey, HashInterface $digisetHash)
    {
        $this->privateKey = file_get_contents( $privateKey );
        $this->publicKey = file_get_contents( $publicKey );
        $this->digisetHash = $digisetHash;

        $this->publicKeyResource = openssl_x509_read($this->publicKey);
        $this->privateKeyResource = openssl_pkey_get_private($this->privateKey);
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * @return string
     */
    public function get509() : string
    {
        $certPem = null;
        openssl_x509_export($this->publicKeyResource, $certPem);
        $certContent = str_replace('-----BEGIN CERTIFICATE-----', '', $certPem);
        $certContent = trim(str_replace('-----END CERTIFICATE-----', '', $certContent));
        return $certContent;
    }

    /**
     * @return string
     */
    public function get509DigestValue()
    {
        $certContent = $this->get509();
        return base64_encode($this->getDigisetHash()->calculate(base64_decode($certContent)));
    }

    /**
     * @return mixed
     */
    public function getSerialNumber()
    {
        $certData = openssl_x509_parse($this->publicKeyResource);
        return $certData['serialNumber'];
    }

    /**
     * @return string
     */
    public function getIssuerName()
    {
        $certData = openssl_x509_parse($this->publicKeyResource);
        $issuerArray = array();
        foreach($certData['issuer'] as $issueKey => $issue) {
            $issuerArray[] = $issueKey . '=' . $issue;
        }

        return implode(',', $issuerArray);
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:KeyInfo xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:KeyInfo>');
        $xml
            ->addChild('ds:X509Data')
            ->addChild('ds:X509Certificate',$this->get509())
        ;

        return $this->convertSimpleXMLElementToDOMDocument($xml);
    }

    /**
     * @return HashInterface
     */
    public function getDigisetHash(): HashInterface
    {
        return $this->digisetHash;
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    /**
     * @param string $data
     * @param int $algorithm
     * @return string
     */
    public function sign(string $data, int $algorithm)
    {
        $binary_signature = "";
        openssl_sign($data, $binary_signature, $this->privateKeyResource, $algorithm);
        return $binary_signature;
    }
}