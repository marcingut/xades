<?php
declare(strict_types=1);


namespace MG\XAdES\SignatureMethod;


class SHA256Signature extends SignatureAbstract
{
    const ALGORITHM = OPENSSL_ALGO_SHA256;
    const ALGORITHM_URL = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

    /**
     * @return int
     */
    public function getAlgorithm()
    {
        return self::ALGORITHM;
    }

    /**
     * @return string
     */
    public function getAlgorithmUrl()
    {
        return self::ALGORITHM_URL;
    }
}