<?php
declare(strict_types=1);


namespace MG\XAdES\SignatureMethod;


use MG\XAdES\XMLNamespaces;

class SHA1Signature extends SignatureAbstract
{
    const ALGORITHM = OPENSSL_ALGO_SHA1;
    const ALGORITHM_URL = XMLNamespaces::DS_URI."#rsa-sha1";

    /**
     * @return int
     */
    public function getAlgorithm()
    {
        return self::ALGORITHM;
    }

    /**
     * @return string
     */
    public function getAlgorithmUrl()
    {
        return self::ALGORITHM_URL;
    }
}