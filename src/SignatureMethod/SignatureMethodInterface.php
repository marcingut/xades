<?php
declare(strict_types=1);


namespace MG\XAdES\SignatureMethod;


use MG\XAdES\Certificate;

interface SignatureMethodInterface
{
    /**
     * SignatureMethodInterface constructor.
     * @param Certificate $certificate
     */
    public function __construct(Certificate $certificate);

    /**
     * @return mixed
     */
    public function getCertificate();

    /**
     * @param string $data
     * @return mixed
     */
    public function sign(string $data);

    /**
     * @return mixed
     */
    public function getAlgorithm();

    /**
     * @return mixed
     */
    public function getAlgorithmUrl();

}