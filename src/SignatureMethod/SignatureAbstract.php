<?php
declare(strict_types=1);


namespace MG\XAdES\SignatureMethod;

use MG\XAdES\Certificate;

abstract class SignatureAbstract implements SignatureMethodInterface
{
    /**
     * @var Certificate
     */
    private $certificate;

    /**
     * @param string $data
     * @return string
     */
    public function sign(string $data)
    {
        return $this->getCertificate()->sign($data, $this->getAlgorithm());
    }

    /**
     * SignatureAbstract constructor.
     * @param Certificate $certificate
     */
    public function __construct(Certificate $certificate)
    {
        $this->certificate = $certificate;
    }

    /**
     * @return Certificate
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

}