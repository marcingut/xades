<?php
declare(strict_types=1);


namespace MG\XAdES;


use DOMDocument;
use SimpleXMLElement;

class QualifyingProperties extends dsAbstract
{
    /**
     * @var dsAbstract
     */
    private $target;


    /**
     * @var SignedProperties[]
     */
    private $signedProperties=[];
    /**
     * @return dsAbstract
     */
    public function getTarget(): dsAbstract
    {
        return $this->target;
    }

    /**
     * @param dsAbstract $target
     * @return $this
     */
    public function setTarget(dsAbstract $target): self
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @param SignedProperties $signedProperties
     * @return $this
     */
    public function addSignedProperties(SignedProperties $signedProperties) : self
    {
        $this->signedProperties[]=$signedProperties;
        return $this;
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<xades:QualifyingProperties xmlns:xades="'.XMLNamespaces::XADES_URI.'#"></xades:QualifyingProperties>');
        $xml->addAttribute("Target", '#'.$this->getTarget()->getId());

        $dom = $this->convertSimpleXMLElementToDOMDocument($xml);

        foreach ($this->getSignedProperties() as $signedProperty)
        {
            $dom_sub = $signedProperty->asXML()->documentElement;
            $dom_sub = $dom->importNode($dom_sub, true);
            $dom->documentElement->appendChild($dom_sub);
        }

        return $dom;
    }

    /**
     * @return SignedProperties[]
     */
    public function getSignedProperties(): array
    {
        return $this->signedProperties;
    }
}