<?php
declare(strict_types=1);


namespace MG\XAdES\Object;


use MG\XAdES\IdableInterface;
use MG\XAdES\XMLableInterface;

interface ObjectInterface extends IdableInterface, XMLableInterface
{
    /**
     * @return string
     */
    public function getContent(): string;
}