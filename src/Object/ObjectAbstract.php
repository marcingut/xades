<?php
declare(strict_types=1);


namespace MG\XAdES\Object;


use MG\XAdES\dsAbstract;

abstract class ObjectAbstract extends dsAbstract implements ObjectInterface
{

}