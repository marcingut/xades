<?php
declare(strict_types=1);


namespace MG\XAdES\Object;


use MG\XAdES\QualifyingProperties;
use DOMDocument;
use MG\XAdES\XMLNamespaces;
use SimpleXMLElement;

class QualifyingPropertiesObject extends ObjectAbstract
{
    /**
     * @var QualifyingProperties
     */
    private $qualifyingProperties;

    /**
     * QualifyingPropertiesObject constructor.
     * @param QualifyingProperties $qualifyingProperties
     */
    public function __construct(QualifyingProperties $qualifyingProperties)
    {
        $this->qualifyingProperties = $qualifyingProperties;
        parent::__construct();
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:Object xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:Object>');
        $xml->addAttribute("Id", $this->getId());

        $dom = $this->convertSimpleXMLElementToDOMDocument($xml);

        $dom_sub = $this->getQualifyingProperties()->asXML()->documentElement;
        $dom_sub = $dom->importNode($dom_sub, true);
        $dom->documentElement->appendChild($dom_sub);

        return $dom;
    }

    public function getContent(): string
    {
        return $this->asXMLString();
    }

    /**
     * @return QualifyingProperties
     */
    public function getQualifyingProperties(): QualifyingProperties
    {
        return $this->qualifyingProperties;
    }
}