<?php
declare(strict_types=1);


namespace MG\XAdES\Object;


use DOMDocument;
use MG\XAdES\XMLNamespaces;
use SimpleXMLElement;

class ContentObject extends ObjectAbstract
{
    /**
     * @var string
     */
    private $encoding;
    /**
     * @var string
     */
    private $mimeType;
    /**
     * @var string
     */
    private $content;

    /**
     * @return string
     */
    public function getEncoding(): string
    {
        return $this->encoding;
    }

    /**
     * @param string $encoding
     * @return $this
     */
    public function setEncoding(string $encoding): self
    {
        $this->encoding = $encoding;
        return $this;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     * @return $this
     */
    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:Object xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:Object>');
        $xml->addAttribute("Id", $this->getId());
        $xml->addAttribute("Encoding", $this->getEncoding());
        $xml->addAttribute("MimeType", $this->getMimeType());
        $xml->addChild('', $this->getContent());

        return $this->convertSimpleXMLElementToDOMDocument($xml);
    }
}