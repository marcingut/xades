<?php
declare(strict_types=1);


namespace MG\XAdES;


use DateTimeInterface;
use DOMDocument;
use SimpleXMLElement;

class SignedProperties extends dsAbstract
{
    const DATETIME_FORMAT = 'Y-m-d\TH:i:s\Z';
    /**
     * @var dsAbstract
     */
    private $objectReference;
    /**
     * @var mixed|null
     */
    private $encoding;
    /**
     * @var string
     */
    private $mimeType;
    /**
     * @var string|null
     */
    private $description;
    /**
     * @var Certificate
     */
    private $certificate;
    /**
     * @var DateTimeInterface
     */
    private $signedTime;

    /**
     * SignedProperties constructor.
     * @param Certificate $certificate
     * @param IdableInterface $objectReference
     * @param string $mimeType
     * @param null $encoding
     * @param string|null $description
     */
    public function __construct(Certificate $certificate, IdableInterface $objectReference, DateTimeInterface $signedTime, string $mimeType = 'text/plain', $encoding = null, string $description=null)
    {
        $this->certificate = $certificate;
        $this->objectReference = $objectReference;
        $this->encoding = $encoding;
        $this->mimeType = $mimeType;
        $this->signedTime = $signedTime;
        $this->description = $description;

        parent::__construct();
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<xades:SignedProperties xmlns:xades="'.XMLNamespaces::XADES_URI.'#"></xades:SignedProperties>');
        $xml->addAttribute("Id", $this->getId());

        $signedSignatureProperties = $xml->addChild('xades:SignedSignatureProperties',null);

        $signedSignatureProperties->addChild('xades:SigningTime',$this->getSignedTime()->format(self::DATETIME_FORMAT));
        $cert = $signedSignatureProperties
            ->addChild('xades:SigningCertificate')
            ->addChild('xades:Cert');

        $certDigest = $cert->addChild('xades:CertDigest');
        $certDigest->addChild('ds:DigestMethod')->addAttribute('Algorithm',$this->getCertificate()->getDigisetHash()->getAlgorithmUrl());
        $certDigest->addChild('ds:DigestValue',$this->getCertificate()->get509DigestValue());

        $issuerSerial = $cert->addChild('xades:IssuerSerial');
        $issuerSerial->addChild('ds:X509IssuerName', $this->getCertificate()->getIssuerName());
        $issuerSerial->addChild('ds:X509SerialNumber',$this->getCertificate()->getSerialNumber());

        $signedDataObjectProperties = $xml->addChild('xades:SignedDataObjectProperties');
        $dataObjectFormat = $signedDataObjectProperties->addChild('xades:DataObjectFormat');
        $dataObjectFormat->addAttribute('ObjectReference', '#'.$this->getObjectReference()->getId());
        $dataObjectFormat->addChild('xades:MimeType', $this->mimeType);

        return $this->convertSimpleXMLElementToDOMDocument($xml);
    }

    /**
     * @return DateTimeInterface
     */
    public function getSignedTime(): DateTimeInterface
    {
        return $this->signedTime;
    }

    /**
     * @param DateTimeInterface $signedTime
     * @return SignedProperties
     */
    public function setSignedTime(DateTimeInterface $signedTime): SignedProperties
    {
        $this->signedTime = $signedTime;
        return $this;
    }

    /**
     * @return Certificate
     */
    public function getCertificate(): Certificate
    {
        return $this->certificate;
    }

    /**
     * @return dsAbstract
     */
    public function getObjectReference(): dsAbstract
    {
        return $this->objectReference;
    }
}