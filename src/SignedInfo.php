<?php
declare(strict_types=1);


namespace MG\XAdES;


use MG\XAdES\SignatureMethod\SignatureMethodInterface;
use DOMDocument;
use SimpleXMLElement;

class SignedInfo extends dsAbstract
{
    /**
     * @var SignatureMethodInterface
     */
    private $signatureMethod;
    /**
     * @var CanonicalizationMethod
     */
    private $canonicalizationMethod;
    /**
     * @var Reference[]
     */
    private $references=[];

    public function __construct(CanonicalizationMethod $canonicalizationMethod, SignatureMethodInterface $signatureMethod)
    {
        $this->canonicalizationMethod = $canonicalizationMethod;
        $this->signatureMethod = $signatureMethod;

        parent::__construct();
    }

    /**
     * @param Reference $reference
     * @return $this
     */
    public function addReference(Reference $reference) : self
    {
        $this->references[] = $reference;
        return $this;
    }

    /**
     * @return CanonicalizationMethod
     */
    public function getCanonicalizationMethod(): CanonicalizationMethod
    {
        return $this->canonicalizationMethod;
    }

    /**
     * @return SignatureMethodInterface
     */
    public function getSignatureMethod(): SignatureMethodInterface
    {
        return $this->signatureMethod;
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:SignedInfo xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:SignedInfo>');
        $xml->addChild('ds:CanonicalizationMethod')->addAttribute('Algorithm',$this->getCanonicalizationMethod()->getAlgorithmUrl());
        $xml->addChild('ds:SignatureMethod')->addAttribute('Algorithm', $this->getSignatureMethod()->getAlgorithmUrl());

        $dom = $this->convertSimpleXMLElementToDOMDocument($xml);
        foreach ($this->getReferences() as $reference)
        {
            $dom_sub = $reference->asXML()->documentElement;
            $dom_sub = $dom->importNode($dom_sub, true);
            $dom->documentElement->appendChild($dom_sub);
        }

        return $dom;
    }

    /**
     * @return Reference[]
     */
    public function getReferences(): array
    {
        return $this->references;
    }
}