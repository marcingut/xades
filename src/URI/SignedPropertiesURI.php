<?php
declare(strict_types=1);


namespace MG\XAdES\URI;


use MG\XAdES\SignedProperties;

class SignedPropertiesURI implements URIInterface
{
    /**
     * @var SignedProperties
     */
    private $signedProperties;

    /**
     * SignedPropertiesURI constructor.
     * @param SignedProperties $signedProperties
     */
    public function __construct(SignedProperties $signedProperties)
    {
        $this->signedProperties = $signedProperties;
    }

    /**
     * @return string
     */
    public function getURI() : string
    {
        return '#'.$this->getSignedProperties()->getId();
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->getSignedProperties()->asXMLString();
    }

    /**
     * @return SignedProperties
     */
    public function getSignedProperties(): SignedProperties
    {
        return $this->signedProperties;
    }
}