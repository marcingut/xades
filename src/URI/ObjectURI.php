<?php
declare(strict_types=1);


namespace MG\XAdES\URI;

use MG\XAdES\Object\ObjectInterface;

class ObjectURI implements URIInterface
{
    /**
     * @var ObjectInterface
     */
    private $object;

    /**
     * ObjectURI constructor.
     * @param ObjectInterface $object
     */
    public function __construct(ObjectInterface $object)
    {
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getURI() : string
    {
        return '#'.$this->getObject()->getId();
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->getObject()->getContent();
    }

    /**
     * @return ObjectInterface
     */
    public function getObject(): ObjectInterface
    {
        return $this->object;
    }
}