<?php
declare(strict_types=1);


namespace MG\XAdES\URI;


interface URIInterface
{
    /**
     * @return string
     */
    public function getURI() : string;

    /**
     * @return string
     */
    public function getValue() : string;
}