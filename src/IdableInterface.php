<?php
declare(strict_types=1);


namespace MG\XAdES;


interface IdableInterface
{
    /**
     * @return string
     */
    public function getId(): string;
}