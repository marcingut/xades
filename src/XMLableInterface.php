<?php
declare(strict_types=1);


namespace MG\XAdES;


use DOMDocument;

interface XMLableInterface
{
    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument;

    /**
     * @return string
     */
    public function asXMLString() : string;
}