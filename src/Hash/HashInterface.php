<?php
declare(strict_types=1);


namespace MG\XAdES\Hash;


interface HashInterface
{
    /**
     * @param string $data
     * @return string
     */
    public function calculate(string $data) : string;

    /**
     * @return string
     */
    public function getAlgorithmUrl() : string;
}