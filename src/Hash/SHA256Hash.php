<?php
declare(strict_types=1);


namespace MG\XAdES\Hash;


class SHA256Hash implements HashInterface
{
    const ALGORITHM_URL = 'http://www.w3.org/2001/04/xmlenc#sha256';

    /**
     * @inheritDoc
     */
    public function calculate(string $data): string
    {
        return hash("sha256", $data,true);
    }

    /**
     * @inheritDoc
     */
    public function getAlgorithmUrl(): string
    {
        return self::ALGORITHM_URL;
    }
}