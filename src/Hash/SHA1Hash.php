<?php
declare(strict_types=1);


namespace MG\XAdES\Hash;


use MG\XAdES\XMLNamespaces;

class SHA1Hash implements HashInterface
{
    const ALGORITHM_URL = XMLNamespaces::DS_URI.'#sha1';

    /**
     * @inheritDoc
     */
    public function calculate(string $data): string
    {
        return sha1($data, true);
    }

    /**
     * @inheritDoc
     */
    public function getAlgorithmUrl(): string
    {
        return self::ALGORITHM_URL;
    }
}