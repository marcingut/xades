<?php
declare(strict_types=1);


namespace MG\XAdES;


use MG\XAdES\Hash\HashInterface;
use MG\XAdES\Transform\TransformInterface;
use MG\XAdES\URI\SignedPropertiesURI;
use MG\XAdES\URI\URIInterface;
use DOMDocument;
use SimpleXMLElement;

/**
 * Class Reference
 * @package MG\XAdES
 */
class Reference extends dsAbstract
{
    /**
     * @var URIInterface
     */
    private $URI;
    /**
     * @var string
     */
    private $type;
    /**
     * @var HashInterface
     */
    private $digestMethod;
    /**
     * @var TransformInterface[]
     */
    private $transforms=[];

    /**
     * Reference constructor.
     * @param URIInterface $URI
     * @param HashInterface $digestMethod
     */
    public function __construct(URIInterface $URI, HashInterface $digestMethod)
    {
        if ($URI instanceof SignedPropertiesURI)
        {
            $this->type = 'http://uri.etsi.org/01903#SignedProperties';
        }

        $this->URI = $URI;
        $this->digestMethod = $digestMethod;

        parent::__construct();
    }

    /**
     * @return string
     */
    public function getURI(): string
    {
        return $this->URI->getURI();
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return HashInterface
     */
    public function getDigestMethod(): HashInterface
    {
        return $this->digestMethod;
    }

    /**
     * @return string
     */
    public function getDigestValue(): string
    {
        $value = $this->URI->getValue();
        if (count($this->transforms)>0)
        {
            foreach ($this->transforms as $transform)
            {
                $value = $transform->transform($value);
            }
        }
        $digestValue=($this->getDigestMethod()->calculate($value));
        return base64_encode($digestValue);
    }

    public function addTransform(TransformInterface $transform) : self
    {
        $this->transforms[]=$transform;
        return $this;
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:Reference xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:Reference>');
        $xml->addAttribute("Id", $this->getId());

        if (!is_null($this->getType()))
        {
            $xml->addAttribute("Type", $this->getType());
        }

        if (!is_null($this->getURI()))
        {
            $xml->addAttribute("URI", $this->getURI());
        }

        $xml->addChild('ds:DigestMethod')->addAttribute('Algorithm',$this->getDigestMethod()->getAlgorithmUrl());
        $xml->addChild('ds:DigestValue',$this->getDigestValue());

        if (count($this->transforms)>0)
        {
            $transformsXml = $xml->addChild('ds:Transforms');

            foreach ($this->transforms as $transform)
            {
                $transformsXml->addChild('ds:Transform')->addAttribute('Algorithm', $transform->getAlgorithmUrl());
            }
        }

        return $this->convertSimpleXMLElementToDOMDocument($xml);
    }
}