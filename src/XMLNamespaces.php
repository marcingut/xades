<?php
declare(strict_types=1);


namespace MG\XAdES;


class XMLNamespaces
{
    const XADES_URI = 'http://uri.etsi.org/01903/v1.3.2';
    const DS_URI = 'http://www.w3.org/2000/09/xmldsig';
}