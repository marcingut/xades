<?php
declare(strict_types=1);


namespace MG\XAdES;
use DOMDocument;
use SimpleXMLElement;


class SignatureValue extends dsAbstract
{
    /**
     * @var SignedInfo
     */
    private $signedInfo;

    /**
     * SignatureValue constructor.
     * @param SignedInfo $signedInfo
     */
    public function __construct(SignedInfo $signedInfo)
    {
        $this->signedInfo = $signedInfo;
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        $xml = $this->getSignedInfo()->asXML();

        $data = $this->getSignedInfo()->getCanonicalizationMethod()->getCanonicalized($xml);

        return base64_encode($this->getSignedInfo()->getSignatureMethod()->sign($data));
    }

    /**
     * @return DOMDocument
     */
    public function asXML() : DOMDocument
    {
        $xml = new SimpleXMLElement('<ds:SignatureValue xmlns:ds="'.XMLNamespaces::DS_URI.'#"></ds:SignatureValue>');
        $xml->addAttribute("Id", $this->getId());
        $xml[0]=$this->getValue();

        return $this->convertSimpleXMLElementToDOMDocument($xml);
    }

    /**
     * @return SignedInfo
     */
    public function getSignedInfo(): SignedInfo
    {
        return $this->signedInfo;
    }
}