<?php
declare(strict_types=1);


namespace MG\XAdES;


use DOMDocument;
use SimpleXMLElement;
use Symfony\Component\Uid\Uuid;

abstract class dsAbstract implements IdableInterface, XMLableInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * dsAbstract constructor.
     */
    public function __construct()
    {
        $this->id = 'ID-'.Uuid::v4();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function asXMLString() : string
    {
        return $this->asXML()->saveXML();
    }

    protected function convertSimpleXMLElementToDOMDocument(SimpleXMLElement $simpleXMLElement) : DOMDocument
    {
        $xml_s = dom_import_simplexml($simpleXMLElement);
        $dom = new DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml_s = $dom->importNode($xml_s, true);
        $dom->appendChild($xml_s);

        return $dom;
    }
}