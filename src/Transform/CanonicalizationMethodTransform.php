<?php
declare(strict_types=1);


namespace MG\XAdES\Transform;


use MG\XAdES\CanonicalizationMethod;
use DOMDocument;

class CanonicalizationMethodTransform implements TransformInterface
{
    /**
     * @var CanonicalizationMethod
     */
    private $canonicalizationMethod;

    /**
     * CanonicalizationMethodTransform constructor.
     */
    public function __construct()
    {
        $this->canonicalizationMethod = new CanonicalizationMethod();
    }

    /**
     * @return string
     */
    public function getAlgorithmUrl()
    {
        return $this->getCanonicalizationMethod()->getAlgorithmUrl();
    }

    /**
     * @param string $value
     * @return string
     */
    public function transform(string $value) : string
    {
        $dom = new DOMDocument();
        $dom->loadXML($value);
        return $this->getCanonicalizationMethod()->getCanonicalized($dom);
    }

    /**
     * @return CanonicalizationMethod
     */
    public function getCanonicalizationMethod(): CanonicalizationMethod
    {
        return $this->canonicalizationMethod;
    }
}