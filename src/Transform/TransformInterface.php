<?php
declare(strict_types=1);


namespace MG\XAdES\Transform;


interface TransformInterface
{
    /**
     * @return mixed
     */
    public function getAlgorithmUrl();

    /**
     * @param string $value
     * @return string
     */
    public function transform(string $value) : string;
}